FROM python:3.9.6-slim

RUN apt-get update && \
    apt-get install -y \
      curl \
      wget \
      git \
      nano

CMD ["python", "main.py"]
